$(call inherit-product, vendor/ose/config/common_full_phone.mk)
$(call inherit-product, vendor/ose/config/nfc_enhanced.mk)

# Boot animation
TARGET_SCREEN_WIDTH := 720
TARGET_SCREEN_HEIGHT := 1280

$(call inherit-product, device/motorola/moto_msm8960/full_moto_msm8960.mk)

# Release name
PRODUCT_NAME := ose_moto_msm8960